# Intelligent Virtual Coach for Self-training Manual Skills in Laparoscopy

## -- Project Status: [On-Hold]

## Project Description
This project is the data repository of Intelligent Virtual Coach for Self-training Manual Skills in Laparoscopy.

## Licence
[Licence](/LICENSE)

## Data Description
* Three types of data: eye gaze data, operation videos, and trial logging data.

#### Name of the folder
* YYYYMMDD_PID-EXPNO
* PID: each participant has an identical index.
* EXPNO: data of some participants may be stored in two folders.

### Eye gaze data

#### Name of the `Copy of Tobii*.txt` file
* Eye gaze data
* Format `Copy of Tobii_task{1}_trial{2}_{3}.txt`, where 
* {1} is the index of task ['A','B','C','D','E']
* {2} is the number of trial
* {3} is the UnixTimestamp of the file created
* An example `Copy of Tobii_taskA_trial1_1580769656501.txt`

#### Content of the 'Copy of Tobii*.txt' file
* Usage: can be loaded as .csv file, each column is delimited by a comma
* Using Tobii.Research .NET [read more](https://developer.tobiipro.com/NET/dotnet-sdk-reference-guide.html)
```
public class GazeDataEventArgs : EventArgs
{
    public GazeDataEventArgs(EyeData leftEye, EyeData rightEye, long deviceTimestamp, long systemTimestamp);
    public EyeData LeftEye { get; }
    public EyeData RightEye { get; }
    public long DeviceTimeStamp { get; }
    public long SystemTimeStamp { get; }
}
```
* DEVICE: 
    * Mark
    * value of `Valid` or `Invalid`
    * Mark the data status
    * `LeftEye.GazeOrigin.Validity` 
    * Value: `Valid` or `Invalid`
* X,Y,Z
    * The `X,Y,Z` position in user coordinates [read more about Tobii coordinate systems](https://developer.tobiipro.com/commonconcepts/coordinatesystems.html)
    * X: `LeftEye.GazeOrigin.PositionInUserCoordinates.X`
    * Y: `LeftEye.GazeOrigin.PositionInUserCoordinates.Y`
    * Z: `LeftEye.GazeOrigin.PositionInUserCoordinates.Z`
    * Invalid data is marked as `NaN,NaN,NaN`
    * Example: `11.15395,8.023373,642.1105`
* LPDA_X,LPDA_Y
    * Left eye's gaze point position in 2D on the active display area
    * LPDA_X: `LeftEye.GazePoint.PositionOnDisplayArea.X`
    * LPDA_Y: `LeftEye.GazePoint.PositionOnDisplayArea.Y`
    * Invalide value: `NaN`
    * Example: `0.5626503`
    * [readmore about Tobii .NET SDK](https://developer.tobiipro.com/NET/dotnet-sdk-reference-guide.html)
* RPDA_X,RPDA_Y,
    * Right eye's gaze point position in 2D on the active display area
    * RPDA_X:`RightEye.GazePoint.PositionOnDisplayArea.X`
    * RPDA_Y:`RightEye.GazePoint.PositionOnDisplayArea.Y`
    * Invalide value: `NaN`
    * Example: `0.5626503`
* Pupil_VA,
    * The validity of the pupil data
    * Pupil_VA:`LeftEye.Pupil.Validity`
    * Value: `Valid` or `Invalid`
* Pupil_left,
    * The diameter of the left pupil in millimeters
    * Pupil_left: `LeftEye.Pupil.PupilDiameter`
    * Invalide value: `-1`
    * Example: `3.559143`
* Pupil_right,
    * The diameter of the right pupil in millimeters
    * Pupil_right: `RightEye.Pupil.PupilDiameter`
    * Invalide value: `-1`
    * Example: `3.565964`
* UnixTS,
    * UnixTime
    * `DateTimeOffset(DateTime.UtcNow).ToUnixTimeMilliseconds().ToString()`
    * Example: `1579732062198`
* TimeStamp
    * Time in normal format
    * `DateTimeOffset.Now.ToString("MM/dd/yyyy hh:mm:ss.fff").ToString()`
    * Example: `01/22/2020 05:27:42.198`
* Example code C#
```{c#}
var UnixTimestamp = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeMilliseconds().ToString();
var local_timestamp = DateTimeOffset.Now.ToString("MM/dd/yyyy hh:mm:ss.fff").ToString();

var t_str = String.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12}\r\n",
    e.LeftEye.GazeOrigin.Validity,
    e.LeftEye.Pupil.PupilDiameter,
    e.RightEye.Pupil.PupilDiameter,
    UnixTimestamp,
local_timestamp);
    local_timestamp);
```

### Operation videos

#### Name of the `*.avi` file
* Video
* Format `Copy of Camera3_task{1}_trial{2}_{3}.avi`, where 
* {1} is the index of task ['A','B','C','D','E']
* {2} is the number of trial
* {3} is the UnixTimestamp of the file created
* An example `Copy of Camera3_taskA_trial3_1579732061672.avi`

### Trial logging data

#### Name of the `Copy of Trials*.txt` file
* Log information of operation
* Format `Copy of Trials_{1}.txt`, where
* {1} is the UnixTimestamp of the file created
* An example `Copy of Trials_1572706093316.txt`

## Getting Started
1. Clone thie rep 
2. Raw data is being kept [link](/Data)
3. Data processing

# Authors
* Nathan K. Lau, Virginia Tech [website](https://sites.google.com/a/vt.edu/vacse/)
* Laura E. Barnes, University of Virginia
* Sarah Henrickson Parker, Virginia Tech
* Shawn D. Safford, Penn State
* Srijith Rajamohan, Databricks
* Chaitanya Kulkarni, Virginia Tech
* Tianzi Wang, Virginia Tech
* Shiyu Deng, Virginia Tech
* Jacob Hartman-Kenzler, Virginia Tech

### Acknowledgement:
This research was supported in part by the National Center for Advancing Translational Sciences of the National Institutes of Health under Award Number UL1TR003015. We thanked the Carilion Clinic personnel who volunteered to support our data collection. 
